package com.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class UserController {
    private AtomicLong counter = new AtomicLong();

    @GetMapping("/hello")
    public String getHelloWordObject() {

        return "In New User: Hi there! you are number " + counter.incrementAndGet();
    }
}
